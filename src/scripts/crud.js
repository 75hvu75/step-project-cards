function createRecord(data) {
    fetch('/api/records', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(data),
    })
    .then(response => response.json())
    .then(newRecord => {
        
        console.log('Новий запис створено:', newRecord);
    })
    .catch(error => {
        console.error('Помилка при створенні запису:', error);
    });
}

function readRecord(id) {
    fetch(`/api/records/${id}`)
    .then(response => response.json())
    .then(record => {
    
        console.log('Зчитаний запис:', record);
    })
    .catch(error => {
        console.error('Помилка при зчитуванні запису:', error);
    });
}


function updateRecord(id, newData) {
    fetch(`/api/records/${id}`, {
        method: 'PUT',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(newData),
    })
    .then(response => response.json())
    .then(updatedRecord => {
        
        console.log('Запис оновлено:', updatedRecord);
    })
    .catch(error => {
        console.error('Помилка при оновленні запису:', error);
    });
}


function deleteRecord(id) {
    fetch(`/api/records/${id}`, {
        method: 'DELETE',
    })
    .then(response => {
        if (response.status === 204) {
            
            console.log('Запис видалено');
        } else {
            console.error('Помилка при видаленні запису');
        }
    })
    .catch(error => {
        console.error('Помилка при видаленні запису:', error);
    });
}