import { sendRequest } from '../../helpers/client.js';
import { API } from '../configs/API.js';
import { storage } from '../classes/Filters.js';

export async function getAllCards() {
  const token = localStorage.getItem('token');
  console.log('Значення токену in getAllCards : ', token);

  try {
    const response = await sendRequest(API, 'GET', {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });

    // if (!!response) {
    if (response instanceof Array) {
      console.log('Отримані картки:', response);
      return response;
    }
  } catch (error) {
    console.error('Помилка при отриманні карток:', error);
    throw error;
  }
}

function createCardHTML(card) {
  return `
		<div id="_${card.id}" class="card-wrapper container col-3 " style="padding: 5px; width: 276px;">
			<div  class="card">
				<div class="card-header d-flex" style="padding: 10px;">
					<h6 class="card-title me-auto">Візит до лікаря: №${card.id}</h6>
					<button type="button" class="delete-button btn-close" data-id="${card.id} data-bs-dismiss="modal" aria-label="Delete"></button>
		        </div>

				<div>
					<div>Статус візиту: 
					<button id="status-button-${card.id}" class="btn btn-success" style="--bs-btn-padding-y: .15rem; --bs-btn-padding-x: .4rem; --bs-btn-font-size: .70rem;">
						OPEN
					</button>
					<button class="btn btn-secondary d-none" style="--bs-btn-padding-y: .15rem; --bs-btn-padding-x: .4rem; --bs-btn-font-size: .70rem;">
						DONE
					</button>
				</div>
				</div>

		    <div class="card-body" style="padding: 10px;">
		      <h5 class="card-title">Лікар: <span id="card-doctor">${card.doctor}</span></h5>
		      <p class="card-text">ПІБ: <span id="card-name">${card.name}</span></p>
					
					<div id="additionalInfo" class="collapse">
						<p class="card-text">Мета візиту: <span id="card-purpose">${card.purpose}</span></p>
						<p class="card-text">Короткий опис візиту: <span id="card-description">${card.description}</span></p>
						<p class="card-text">Терміновість: <span id="card-urgency">${card.urgency}</span></p>
		        <p class="card-text">Звичайний тиск: <span id="card-pressure">${card.pressure}</span></p>
		        <p class="card-text">Індекс маси тіла: <span id="card-index">${card.index}</span></p>
		        <p class="card-text">Перенесені захворювання серцево-судинної системи: <span id="card-disease">${card.disease}</span></p>
		        <p class="card-text">Вік: <span id="card-age">${card.age}</span></p>
		        <p class="card-text">Дата останнього відвідування: <span id="card-age">${card.lastVisitDate}</span></p>
		        <p class="card-text">Вік: <span id="card-ageTherapist">${card.ageTherapist}</span></p>
		      </div>
		            
					<button id="showMoreButton" class="me-auto btn btn-primary  " data-bs-toggle="collapse" data-bs-target="#additionalInfo" style="--bs-btn-padding-y: .2rem; --bs-btn-padding-x: .4rem; --bs-btn-font-size: .60rem;">
						Показати більше
					</button>
		            <button id="editButton" class="btn btn-warning " style="--bs-btn-padding-y: .25rem; --bs-btn-padding-x: .4rem; --bs-btn-font-size: .60rem;">
						Редагувати
					</button>
		        </div>
			</div>
		</div>    
	`;
}

// Відображення карток на сторінці
function displayCards(cards) {
  const cardsContainer = document.querySelector('.cards');

  // Очищаєння контейнера перед виведенням нових карток
  cardsContainer.innerHTML = '';

  // Перебирання карток та додавання їх до контейнера
  cards.forEach(card => {
    const cardHTML = createCardHTML(card);
    cardsContainer.innerHTML += cardHTML;
  });
}

// Головна функція, яка викликається при завантаженні сторінки
export async function mainCards() {
  try {
    const cards = await getAllCards();
    storage.visits = cards;

    const boardMessage = document.querySelector('.board-message');
    const cardsContainer = document.querySelector('.cards');

    // Перевірка, чи є карти для відображення
    if (cards.length > 0) {
			console.log('cards.length: ', cards.length);
			
      displayCards(cards);
      console.log('cardsContainer: ', cardsContainer);

      boardMessage.style.display = 'none';
      cardsContainer.style.display = 'block';
    } else {
      boardMessage.style.display = 'block';
      cardsContainer.style.display = 'none';
    }
  } catch (error) {
    console.error('Помилка при отриманні та виведенні карток:', error);
  }
}
