import { addClassToElements } from "./addClassToElements.js";

export function selectedDoctorFields() {
    const doctorSelect = document.getElementById('doctorSelect');

    // Визначення полів лікарів
    const createVisitModal = document.getElementById('createVisitModal');
    const commonFields = document.getElementById('commonFields');
    const cardiologistFields = document.getElementById('cardiologistFields');
    const dentistFields = document.getElementById('dentistFields');
    const therapistFields = document.getElementById('therapistFields');
    const listOfFields = [commonFields, cardiologistFields, dentistFields, therapistFields];

    doctorSelect.addEventListener('change', function () {
        let selectedDoctor = doctorSelect.value;

        // Приховування полів лікарів
        addClassToElements(listOfFields, 'd-none');

        // Відображення додаткового блоку з полями відповідного лікаря
        if (selectedDoctor === 'cardiologist') {
            commonFields.classList.remove('d-none');
            cardiologistFields.classList.remove('d-none');
        } else if (selectedDoctor === 'dentist') {
            commonFields.classList.remove('d-none');
            dentistFields.classList.remove('d-none');
        } else if (selectedDoctor === 'therapist') {
            commonFields.classList.remove('d-none');
            therapistFields.classList.remove('d-none');
        }
    });
}
