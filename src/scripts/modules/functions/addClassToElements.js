export function addClassToElements(elements, className) {
    elements.forEach(element => {
        element.classList.add(className);
    });
}
