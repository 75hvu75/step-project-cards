import { API } from '../configs/API.js';
import { clearForm } from './clearForm.js';
import { addClassToElements } from './addClassToElements.js';
import { storage } from '../classes/Filters.js';


// const token = localStorage.getItem('token');
// console.log('Значення Tокену в createNewCard.js : ', token);

export const listOfFields = [commonFields, cardiologistFields, dentistFields, therapistFields];

export async function createNewCard() {
	const token = localStorage.getItem('token');
	console.log('token in createNewCard : ', token);


    const createVisitButton = document.getElementById('createVisitButton');

    createVisitButton.addEventListener('click', async (e) => {
        e.preventDefault();

        const doctorSelect = document.getElementById('doctorSelect').value;
        const purpose = document.getElementById('purpose').value;
        const description = document.getElementById('description').value;
        const urgencySelect = document.getElementById('urgencySelect').value;
        const name = document.getElementById('name').value;

        const data = {
            doctor: doctorSelect,
            purpose: purpose,
            description: description,
            urgency: urgencySelect,
            name: name,
        };

        if (doctorSelect === 'cardiologist') {
            const pressure = document.getElementById('pressure').value;
            const index = document.getElementById('index').value;
            const disease = document.getElementById('disease').value;
            const age = document.getElementById('age').value;

            data.pressure = pressure;
            data.index = index;
            data.disease = disease;
            data.age = age;
        } else if (doctorSelect === 'dentist') {
            const lastVisitDate = document.getElementById('lastVisitDate').value;

            data.lastVisitDate = lastVisitDate;
        } else if (doctorSelect.value === 'therapist') {
            const ageTherapist = document.getElementById('ageTherapist').value;

            data.ageTherapist = ageTherapist;
        }

        try {
            const response = await fetch(API, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${token}`,
                },
                body: JSON.stringify(data),
            });

            if (response.ok) {
                const responseData = await response.json();
                console.log('Карта була успішно створена', responseData);

                // Очищенняполя форми після створення карти
                clearForm();
                addClassToElements(listOfFields, 'd-none');
            } else {
                console.error('Помилка при створенні карти');
            }
        } catch (error) {
            console.error('Помилка при виконанні запиту:', error);
        }
    });

    // Отримання кнопки закриття модального вікна
    const closeButton = document.querySelector('.btn-close');

    // Обробник події на кнопку закриття
    closeButton.addEventListener('click', () => {
        // Очищення поля форми
        clearForm();
        addClassToElements(listOfFields, 'd-none');
    });

    // Обробник події для закриття форми кліком поза формою
    window.addEventListener('click', event => {
        if (event.target === createVisitModal) {
            clearForm();
            addClassToElements(listOfFields, 'd-none');
        }
    });
}
