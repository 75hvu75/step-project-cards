export function clearForm() {
    const clearableFields = document.querySelectorAll('[data-clearable]');
    clearableFields.forEach(field => (field.value = ''));
}
