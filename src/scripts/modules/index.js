// import { selectedDoctorFields } from './functions/selectedDoctorFields.js';
// import { createNewCard } from './functions/createNewCard.js';
// import { main } from './functions/getAllCards.js';
import { filters } from './classes/Filters.js';

// Цей код буде виконуватися після завантаження сторінки
document.addEventListener('DOMContentLoaded', () => {
// 	main();
//     selectedDoctorFields();
//     createNewCard();

    filters.showFilters();
    filters.filterByAll();
});
