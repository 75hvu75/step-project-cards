export class Visit {
    constructor(user) {
		const { doctor, purpose, description, urgency, name } = user;
		this.doctor = doctor;
        this.purpose = purpose;
        this.description = description;
        this.urgency = urgency;
        this.name = name;
    }
}
