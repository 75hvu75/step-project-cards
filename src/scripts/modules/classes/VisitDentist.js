import Visit from "./Visit";

export default class VisitDentist extends Visit {
    constructor(user, data) {
        super(user);
        this.data = data;
    }
}
