import Visit from "./Visit";

export default class VisitTherapist extends Visit {
    constructor(user, ageTherapist) {
        super(user);
        this.ageTherapist = ageTherapist;
    }
}
