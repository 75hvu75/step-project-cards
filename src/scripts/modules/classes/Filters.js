export const storage = {
		authToken: null,
	visits: [],
	filteredVisits: []
}
// console.log(storage);

export class Filters {
    constructor() {
        this.filterBtn = document.querySelector('#filter-button');
        this.filterPurpose = document.querySelector('#filter-purpose');
        this.form = document.querySelector('#filter');
        this.filterDoctor = document.querySelector('#filter-doctor');
        this.filterUrgency = document.querySelector('#filter-urgency');
        this.filterStatus = document.querySelector('#filter-status');
	}
	

    showFilters() {
        this.filterBtn.addEventListener('click', () => {
            this.form.classList.toggle('d-none');
			if (window.innerWidth >= 1200) {
				this.form.classList.toggle('d-xl-flex', !this.form.classList.contains('d-none'));
			} else if (window.innerWidth >= 992) {
                this.form.classList.toggle('d-lg-flex');
            } else if (window.innerWidth >= 768) {
				this.form.classList.toggle('d-md-flex', !this.form.classList.contains('d-none'));
			}
        });
	}

	hideVisits() {
        const cardWrapper = document.querySelectorAll('.card-wrapper');

        cardWrapper.forEach(item => (item.style.display = 'none'));		
		storage.filteredVisits.forEach(({ id }) => (document.querySelector(`#_${id}`).style.display = 'block'));
    }

	
	filterFunction() {
		storage.filteredVisits = [];
        const noSearchString = ({ searhcString, ...rest }) => rest;
        const whereFind = ['purpose', 'description'];
        let whatFind = {};
        let res = [];

        const formData = new FormData(this.form);
        for (const [key, value] of formData.entries()) {
            if (!!value && value !== 'none') {
                whatFind[key] = value;
            }
		}
		
		const searchString = whatFind['searhcString'];

        if (!!searchString) {
            res = [];
            whatFind = noSearchString(whatFind);

            storage.visits.forEach(item => {
                for (const [key, value] of Object.entries(item)) {
                    if (new RegExp(searchString, 'i').test(value) && whereFind.includes(key)) {
                        res.push(item);
                        return;
                    }
                }
            });

            if (!res.length) {
                storage.filteredVisits = [];
                this.hideVisits();
                this.showInformText();
                return false;
            }
        }

        (res.length > 0 ? res : storage.visits).forEach(obj => {
            for (const [key, value] of Object.entries(whatFind)) {
                if (!obj.hasOwnProperty(key) || String(obj[key]) !== String(value)) {
                    return;
                }
            }
            storage.filteredVisits.push(obj);
        });
		console.log(storage.filteredVisits);
        this.hideVisits();

        if (!storage.filteredVisits.length) {
            this.showInformText();
        }
	}

	filterByAll() {
        this.filterDoctor.addEventListener('change', () => {
            this.filterFunction();
        });
        this.filterUrgency.addEventListener('change', () => {
            this.filterFunction();
        });
        this.filterStatus.addEventListener('change', () => {
            this.filterFunction();
        });

        this.filterPurpose.addEventListener('input', () => {
            this.filterFunction();
        });
    }

    showInformText() {
        const informBar = document.querySelector('.inform-bar');

        informBar.textContent = 'Нічого не знайдено';

        setTimeout(() => {
            informBar.textContent = '';
        }, 2000);
    }
}

export const filters = new Filters();
