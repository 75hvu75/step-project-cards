import Visit from "./Visit";

export default class VisitCardiologist extends Visit {
    constructor(user, pressure, index, disease, age) {
        super(user);
        this.pressure = pressure;
        this.index = index;
        this.disease = disease;
        this.age = age;
    }
}
