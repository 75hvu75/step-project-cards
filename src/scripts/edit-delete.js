function addNewCard() {
    const doctorSelect = document.getElementById('doctorSelect');
    const purpose = document.getElementById('purpose');
    const description = document.getElementById('description');
    const urgencySelect = document.getElementById('urgencySelect');
    const name = document.getElementById('name');

    const commonFields = document.getElementById('commonFields');
    const cardiologistFields = document.getElementById('cardiologistFields');
    const dentistFields = document.getElementById('dentistFields');
    const therapistFields = document.getElementById('therapistFields');

    const newCard = document.createElement('div');
    newCard.classList.add('card');

    let cardContent = `
        <p>Лікар: ${doctorSelect.value}</p>
        <p>Мета візиту: ${purpose.value}</p>
        <p>Короткий опис візиту: ${description.value}</p>
        <p>Терміновість: ${urgencySelect.value}</p>
        <p>ПІБ: ${name.value}</p>
    `;

    if (doctorSelect.value === 'cardiologist') {
        const pressure = document.getElementById('pressure').value;
        const index = document.getElementById('index').value;
        const disease = document.getElementById('disease').value;
        const age = document.getElementById('age').value;

        cardContent += `
            <p>Звичайний тиск: ${pressure}</p>
            <p>Індекс маси тіла: ${index}</p>
            <p>Перенесені захворювання серцево-судинної системи: ${disease}</p>
            <p>Вік: ${age}</p>
        `;
    } else if (doctorSelect.value === 'dentist') {
        const lastVisitDate = document.getElementById('lastVisitDate').value;

        cardContent += `<p>Дата останнього відвідування: ${lastVisitDate}</p>`;
    } else if (doctorSelect.value === 'therapist') {
        const ageTherapist = document.getElementById('ageTherapist').value;

        cardContent += `<p>Вік: ${ageTherapist}</p>`;
    }

    newCard.innerHTML = cardContent;

    const cardsSection = document.querySelector('.cards');
    cardsSection.appendChild(newCard);
    
     const editButton = document.createElement('button');
    editButton.textContent = 'Редагувати';
    editButton.classList.add('edit-button');
    
    const deleteButton = document.createElement('button');
    deleteButton.textContent = 'Видалити';
    deleteButton.classList.add('delete-button');
    
    newCard.appendChild(editButton);
    newCard.appendChild(deleteButton);
}