import LoginModal from '../classes/_login-modal.js';
import { mainCards } from '../modules/functions/getAllCards.js';
import { selectedDoctorFields } from '../modules/functions/selectedDoctorFields.js';
import { createNewCard } from '../modules/functions/createNewCard.js';

const modalElement = document.querySelector('#login-modal');
const loginModal = new LoginModal(modalElement);
const loginButton = document.querySelector('#login');
const cardsContainer = document.querySelector('.cards');

document.addEventListener('DOMContentLoaded', () => {
    loginButton.addEventListener('click', async () => {
		loginModal.submit();
		
        await new Promise(resolve => setTimeout(resolve, 700));
        const token = localStorage.getItem('token');
		console.log('token in _modal: ', token);
		
        mainCards();
        selectedDoctorFields();
        createNewCard();
        console.log('cardsContainer in modal: ', cardsContainer);
    });
});
