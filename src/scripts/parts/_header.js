import HeaderControls from '../classes/_header-controls.js';

const headerControls = new HeaderControls();
document.addEventListener("DOMContentLoaded", () => {
    headerControls.render();
});

document.querySelector('.logout-btn').addEventListener('click', () => {
    headerControls.logout();
});

