export default class HeaderControls {
    constructor() {
        this._loginBtn = document.querySelector('.login-btn');
        this._logoutBtn = document.querySelector('.logout-btn');
        this._createVisitBtn = document.querySelector('.create-visit-btn');
        this._cardsContainer = document.querySelector('.cards');
    }

    render() {
        const token = localStorage.getItem('token');
        console.log('token in render: ', token);
        if (token) {
            this._loginBtn.classList.add('d-none');
            this._createVisitBtn.classList.remove('d-none');
            this._logoutBtn.classList.remove('d-none');
        }
    }

    logout() {
        localStorage.removeItem('token');
        this._loginBtn.classList.remove('d-none');
        this._createVisitBtn.classList.add('d-none');
        this._logoutBtn.classList.add('d-none');
        this._cardsContainer.classList.add('d-none');
    }

    login() {
        this._loginBtn.classList.add('d-none');
        this._createVisitBtn.classList.remove('d-none');
		this._logoutBtn.classList.remove('d-none');
		this._cardsContainer.classList.remove('d-none');

    }
}
