import { sendRequest } from '../helpers/client.js';
import HeaderControls from './_header-controls.js';
const URL_LOGIN = 'https://ajax.test-danit.com/api/v2/cards/login';

export default class LoginModal {
    constructor(modalElement) {
        this._modalElement = modalElement;
        this._emailInput = this._modalElement.querySelector('#email-input');
        this._passwordInput = this._modalElement.querySelector('#password-input');
        this._isValidData = true;
    }

    clear() {
        const invalidFeedbacks = this._modalElement.querySelectorAll('.invalid-feedback');
        invalidFeedbacks.forEach((invalidFeedback) => {
            invalidFeedback.remove();
        });

        this._emailInput.classList.remove('is-invalid');
        this._emailInput.classList.remove('is-valid');

        this._passwordInput.classList.remove('is-invalid');
        this._passwordInput.classList.remove('is-valid');

        this._isValidData = true;

    }

    emailValidation() {
        const email = this._emailInput.value;
        const invalidDiv = document.createElement('div');
        invalidDiv.classList.add('invalid-feedback');

        if (email === '') {
            this._emailInput.classList.add('is-invalid');
            invalidDiv.innerHTML = 'Будь ласка заповніть поле';
            this._emailInput.parentNode.appendChild(invalidDiv);
            this._isValidData = false;
            return;
        }

        const emailRegExp = new RegExp('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$');
        if (!emailRegExp.test(email)) {
            this._emailInput.classList.add('is-invalid');
            invalidDiv.innerHTML = 'Будь ласка введіть коректний email';
            this._emailInput.parentNode.appendChild(invalidDiv);
            this._isValidData = false;
            return;
        }

        this._emailInput.classList.add('is-valid');
    }

    passwordValidation() {
        const password = this._passwordInput.value;
        const invalidDiv = document.createElement('div');
        invalidDiv.classList.add('invalid-feedback');

        if (password === '') {
            this._passwordInput.classList.add('is-invalid');
            invalidDiv.innerHTML = 'Будь ласка заповніть поле';
            this._passwordInput.parentNode.appendChild(invalidDiv);
            this._isValidData = false;
            return;
        }

        const passwordRegExp = new RegExp('^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.{8,})');
        if (!passwordRegExp.test(password)) {
            this._passwordInput.classList.add('is-invalid');
            invalidDiv.innerHTML = 'Пароль повинен містити не менше 8 символів, включаючи одну цифру, одну велику та одну малу літеру';
            this._passwordInput.parentNode.appendChild(invalidDiv);
            this._isValidData = false;
            return;
        }

        this._passwordInput.classList.add('is-valid');
    }

    submit() {
        this.clear();
        this.emailValidation();
        this.passwordValidation();
        if (!this._isValidData) {
           return;
        }

        const email = this._emailInput.value;
        const password = this._passwordInput.value;

        sendRequest(URL_LOGIN, 'POST', {
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({email, password})
        }).then((token) => {
            localStorage.setItem('token', token);
			console.log('localStorage:', token);
            const modal = bootstrap.Modal.getOrCreateInstance(this._modalElement);
            modal.hide();
            const headerControls = new HeaderControls();
            headerControls.login();
            this.clear();
            this._emailInput.value = '';
            this._passwordInput.value = '';
        }).catch(() => {
            const invalidDiv = document.createElement('div');
            invalidDiv.classList.add('invalid-feedback');
            invalidDiv.innerHTML = `
                Помилка авторизації. Перевірте правильність введених даних
            `;
            invalidDiv.style.display = 'block';
            this._modalElement.querySelector('.modal-body').appendChild(invalidDiv);
        });

    }
}
