import pkg from "gulp";
import * as sass from "sass";
import gulpSass from "gulp-sass";
import fileInclude from "gulp-file-include";
import rename from "gulp-rename";
import mode from "gulp-mode";

import browserSync from "browser-sync";
import { deleteAsync } from "del";

import gulpPostcss from "gulp-postcss";
import csscomb from "gulp-csscomb";
import csso from "gulp-csso";
import cssnano from "cssnano";
import flexFix from "postcss-flexbugs-fixes";
import sourcemaps from "gulp-sourcemaps";
import autoprefixer from "autoprefixer";

import prettyHtml from "gulp-pretty-html";
import replace from "gulp-replace";

import imagemin from "gulp-imagemin";
import imageminOptipng from "imagemin-optipng";
import imageminMozjpeg from "imagemin-mozjpeg";
import imageminSvgo from "imagemin-svgo";
import svgSprite from "gulp-svg-sprite";
import webp from "gulp-webp";

import terser from "gulp-terser";
import browserify from "browserify";
import babelify from "babelify";
import source from "vinyl-source-stream";
import buffer from "vinyl-buffer";


import { pngOptions } from "./gulp/configs/pngOptions.js";
import { mozjpegOptions } from "./gulp/configs/mozjpegOptions.js";
import { svgOptions } from "./gulp/configs/svgOptions.js";
import { webpOptions } from "./gulp/configs/webpOptions.js";

const { task, watch, series, src, dest, parallel, lastRun } = pkg;
const scss = gulpSass(sass);

const devMode = mode({
    mode: ["production", "development"],
    default: "development",
    verbose: false
});

task("clean", () => {
    return deleteAsync(["./dist"], { since: lastRun("clean") });
});

task("compileCss", () => {
    const processors = [
        flexFix(),
        autoprefixer({
            grid: true,
            cascade: true
        })
    ];

    return src("./src/styles/*.scss")
        .pipe(scss.sync({
            sourceComments: false,
            outputStyle: "expanded"
        }).on("error", scss.logError))
        .pipe(devMode.production(sourcemaps.init()))
        .pipe(gulpPostcss(processors))
        .pipe(csscomb()) // Format CSS coding style with
        .pipe(devMode.production(csso({
            restructure: true,
            sourceMap: true,
            debug: false
        })))
        .pipe(devMode.production(gulpPostcss([cssnano])))
        .pipe(devMode.production(rename({ suffix: ".min" })))
        .pipe(devMode.production(sourcemaps.write("."))) // => min.style.css
        .pipe(dest("./dist/css/"));
});

task("compileHtml", () => {
    return src("./src/*.html")
        .pipe(devMode.production(replace(/href=".\/css\/style.css"/g, () => {
            return "href=\"./css/style.min.css\""; //
        })))
        .pipe(devMode.production(replace(/src=".\/js\/scripts.js"/g, () => {
            return "src=\"./js/scripts.min.js\""; //
        })))
        .pipe(fileInclude())
        .pipe(prettyHtml({
            indent_size: 4,
            indent_char: " ",
            unformatted: ["code", "pre", "em", "strong", "span", "i", "b", "br"]
        }))
        .pipe(dest("dist"));
});

task("moveImg", () => {
    return src("./src/images/**/*.+(png|jpg|jpeg|svg)", { since: lastRun("moveImg") })
        .pipe(devMode.production(imagemin([
            imageminOptipng({ pngOptions }),
            imageminMozjpeg({ mozjpegOptions }),
            imageminSvgo({ plugins: svgOptions })
        ])))
        .pipe(dest("./dist/images/"));
});

task("image:webp", () => {
    return src("./src/images/**/*.+(png|jpg|jpeg)", { since: lastRun("image:webp") })
        .pipe(webp({ webpOptions }))
        .pipe(dest("./dist/images/"));
});

task("sprite", () => {
    return src([`./src/images/sprites/**/*.svg`, `!./src/images/sprites/sprite.svg`], { since: lastRun("sprite") })
        .pipe(
            svgSprite({
                log: "info",
                shape: {
                    id: {
                        separator: "-",
                        generator: "svg-%s"
                    }
                },
                svg: {
                    transform: [
                        function(svg) {
                            const defsRegex = /<defs[^>]*>.+?<\/defs>/g;
                            const defs = svg.match(defsRegex);

                            if (defs) {
                                svg = svg.replace(defsRegex, "");
                                svg = svg.replace("<symbol ", defs.join("") + "<symbol ");
                            }

                            return svg;
                        }
                    ]
                },
                mode: {
                    symbol: {
                        dest: "",
                        sprite: "./src/images/sprites/sprite.svg",
                        inline: true,
                        render: {
                            scss: {
                                template: "gulp/sprite/tmpl_scss.mustache",
                                dest: "./src/styles/sprites/_sprite.scss"
                            }
                        }
                    }
                },
                variables: {
                    baseFz: 20,
                    prefixStatic: "svg-"
                }
            })
        )
        .pipe(dest("."));
});

task("js", () => {
    return browserify("./src/scripts/app.js")
        .transform(babelify.configure({
            presets: ["@babel/preset-env"]
        }))
        .bundle()
        .pipe(source("scripts.js"))
        .pipe(buffer())
        .pipe(sourcemaps.init({ loadMaps: true }))
        .pipe(devMode.production(terser()))
        .pipe(devMode.production(rename({ suffix: ".min" })))
        .pipe(sourcemaps.write("."))
        .pipe(dest("./dist/js/"));
});

task("serve", () => {
    return browserSync.init({
        server: {
            baseDir: ["dist"]
        },
        port: 9000,
        open: true
    });
});

task("watchers", () => {
    watch("./src/styles/**/*.scss", parallel("compileCss")).on("change", browserSync.reload);
    watch("./src/scripts/**/*.js", parallel("js")).on("change", browserSync.reload);
    watch("./src/**/*.html", parallel("compileHtml")).on("change", browserSync.reload);
    watch("./src/images/**/*.+(png|jpg|jpeg|svg)", parallel("moveImg")).on("change", browserSync.reload);
});

task("build", series("clean", "sprite", parallel("compileCss", "compileHtml", series("moveImg", "image:webp"), "js")));
task("dev", series("build", parallel("serve", "watchers")));


